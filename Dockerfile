FROM mhart/alpine-node:12 

ENV HOME /app
WORKDIR ${HOME}
COPY package.json .babelrc ${HOME}/

RUN npm install --no-production
COPY src/ ${HOME}/src

RUN npm install -g nodemon
CMD nodemon src/app.js --exec ./node_modules/.bin/babel-node -L
