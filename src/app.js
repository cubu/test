import 'core-js/stable';
import 'regenerator-runtime/runtime';
import express from 'express';
import bodyParser from 'body-parser';

const app = express();
const router = new express.Router();

app.use(bodyParser.json());

router.route('/hello').get((req, res) => {
    res.send('ei');
});

router.route('/post').post((req, res) => {
    console.log(req.body);
});

app.use('/', router);

const start = async () => {
    app.listen(5666, () => {
        console.log('Listening at 5666')
    })
};

start();